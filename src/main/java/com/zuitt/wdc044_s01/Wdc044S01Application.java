package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return "Hello " + name + "!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "user") String name) {

		return String.format("Hi %1$s", name);

	}

}
